/*
Copyright 2023 The Sylva authors


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package report

import (
	_ "embed"
	"encoding/json"
	"fmt"
	"os"
	"text/template"
	"time"
)

//go:embed report.tpl.html
var report string

func Generate(data []interface{}, filename string) error {

	if filename == "" {
		filename = fmt.Sprintf("sylva-units-%s.html", time.Now().Format(time.RFC3339))
	}

	textData, err := json.MarshalIndent(data, "", " ")
	if err != nil {
		return err
	}

	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	tpl := template.Must(template.New("base").Parse(string(report)))

	err = tpl.Execute(f, map[string]string{
		"data":    string(textData),
		"endDate": time.Now().Format("2006-01-02T15:04:05"),
	})
	if err != nil {
		return fmt.Errorf("failed to render template: %s", err)
	}
	return nil

}
