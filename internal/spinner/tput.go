package spinner

import (
	"fmt"
	"io"
	"os"

	"golang.org/x/term"
)

// noWrap disables line wrapping in terminal.
func noWrap(w io.Writer) {
	fmt.Fprint(w, "\u001b[?7l")
}

// wrap restores line wrapping in terminal.
func wrap(w io.Writer) {
	fmt.Fprint(w, "\u001b[?7h")
}

// civis hides the cursor.
func civis(w io.Writer) {
	fmt.Fprint(w, "\u001b[?25l")
}

// cnorm shows the cursor.
func cnorm(w io.Writer) {
	fmt.Fprint(w, "\u001b[?25h")
}

// cuu moves the cursor up by n lines.
func cuu(w io.Writer, n int) {
	fmt.Fprintf(w, "\u001b[%dA", n)
}

// clearEOL clear to the end of line
func clearEOL(w io.Writer) {
	fmt.Fprint(w, "\u001b[K")
}

// clearEOS clear to the end of screen
func clearEOS(w io.Writer) {
	fmt.Fprint(w, "\u001b[J")
}

func ttyHeight() int {
	_, height, err := term.GetSize(int(os.Stdout.Fd()))
	if err != nil {
		return 0
	} else {
		return height
	}
}

func IsTerminal() bool {
	return term.IsTerminal(int(os.Stdout.Fd()))
}
