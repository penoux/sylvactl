package informer

import (
	"testing"
)

func TestCleanAnnotations(t *testing.T) {
	annotations := map[string]string{
		"sylvactl/reconcileStartedAt.1.DWjBem":   "2024-03-06T16:47:36",
		"sylvactl/reconcileCompletedAt.1.DWjBem": "2024-03-06T16:49:07",
		"sylvactl/reconcileStartedAt.1.a34GzC":   "2024-03-06T16:51:17",
		"sylvactl/reconcileCompletedAt.1.a34GzC": "2024-03-06T16:52:48",
		"sylvactl/reconcileStartedAt.1.OtrZeW":   "2024-03-06T16:53:17",
		"sylvactl/reconcileCompletedAt.1.OtrZeW": "2024-03-06T16:55:35",
		"sylvactl/reconcileStartedAt.1.HHpEJZ":   "2024-03-06T16:57:17",
		"sylvactl/reconcileCompletedAt.1.HHpEJZ": "2024-03-06T16:59:17",
		"sylvactl/reconcileStartedAt.1.7AScsA":   "2024-03-07T10:46:25",
		"sylvactl/reconcileStartedAt.1.7M7jN7":   "2024-03-07T10:46:29",
		"sylvactl/reconcileStartedAt.1.Iwe0KJ":   "2024-03-07T09:45:00",
		"sylvactl/reconcileStartedAt.1.W9itnk":   "2024-03-07T09:33:21",
		"sylvactl/reconcileStartedAt.1.juwMS9":   "2024-03-07T10:48:54",
	}
	expected_annotations := map[string]string{
		"sylvactl/reconcileCompletedAt.1.a34GzC": "2024-03-06T16:52:48",
		"sylvactl/reconcileStartedAt.1.OtrZeW":   "2024-03-06T16:53:17",
		"sylvactl/reconcileCompletedAt.1.OtrZeW": "2024-03-06T16:55:35",
		"sylvactl/reconcileStartedAt.1.HHpEJZ":   "2024-03-06T16:57:17",
		"sylvactl/reconcileCompletedAt.1.HHpEJZ": "2024-03-06T16:59:17",
		"sylvactl/reconcileStartedAt.1.7AScsA":   "2024-03-07T10:46:25",
		"sylvactl/reconcileStartedAt.1.7M7jN7":   "2024-03-07T10:46:29",
		"sylvactl/reconcileStartedAt.1.Iwe0KJ":   "2024-03-07T09:45:00",
		"sylvactl/reconcileStartedAt.1.W9itnk":   "2024-03-07T09:33:21",
		"sylvactl/reconcileStartedAt.1.juwMS9":   "2024-03-07T10:48:54",
	}
	cleanOldReconcileAnnotations(annotations, ".1.juwMS9")

	for name, expected_value := range expected_annotations {
		if value, ok := annotations[name]; !ok || value != expected_value {
			t.Fatalf("Expected annotation %s not found or not valid in computed annotations", name)
		}
	}
	if len(annotations) != len(expected_annotations) {
		t.Fatal("Cleaned annotations length does not match the expectation")
	}
}
