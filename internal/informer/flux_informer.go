/*
Copyright 2023 The Sylva authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package informer

import (
	"context"
	"fmt"
	"log"
	"sort"
	"strings"
	"sync"
	"time"

	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/dynamic/dynamicinformer"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/util/retry"

	corev1 "k8s.io/api/core/v1"
	apiextensionsv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	apimeta "k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"

	"k8s.io/client-go/tools/cache"
	"sigs.k8s.io/cli-utils/pkg/object"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"github.com/fluxcd/flux2/v2/pkg/manifestgen"
	helmv2 "github.com/fluxcd/helm-controller/api/v2beta2"
	kustomizev1 "github.com/fluxcd/kustomize-controller/api/v1"
	"github.com/fluxcd/pkg/apis/meta"
	sourcev1 "github.com/fluxcd/source-controller/api/v1"
	sourcev1beta2 "github.com/fluxcd/source-controller/api/v1beta2"

	"gitlab.com/sylva-projects/sylva-elements/sylvactl/internal/utils"
)

const (
	DateFormat                   = "2006-01-02T15:04:05"
	maxAnnotations               = 10
	reconcileAnnotationPrefix    = "sylvactl/"
	ReconcileStartedAnnotation   = reconcileAnnotationPrefix + "reconcileStartedAt"
	ReconcileCompletedAnnotation = reconcileAnnotationPrefix + "reconcileCompletedAt"
)

type TreeInformer struct {
	context.Context
	client.Client
	Initialized         bool
	ReconcileDependents bool
	lock                sync.Mutex
	ObjectMap
	ObjEvents        chan ObjEvent
	eventQueue       chan informerEvent
	defaultNamespace string
	runIdentifier    string
}

type ObjEvent struct {
	FluxObject
	Delete bool
}

type informerEvent struct {
	object interface{}
	delete bool
}

var wathedResources = []schema.GroupVersionResource{
	kustomizev1.GroupVersion.WithResource("kustomizations"),
	helmv2.GroupVersion.WithResource("helmreleases"),
	sourcev1.GroupVersion.WithResource("gitrepositories"),
	sourcev1beta2.GroupVersion.WithResource("helmrepositories"),
	sourcev1beta2.GroupVersion.WithResource("helmcharts"),
	sourcev1beta2.GroupVersion.WithResource("ocirepositories"),
}

func NewScheme() *runtime.Scheme {
	scheme := runtime.NewScheme()
	_ = corev1.AddToScheme(scheme)
	_ = apiextensionsv1.AddToScheme(scheme)
	_ = kustomizev1.AddToScheme(scheme)
	_ = helmv2.AddToScheme(scheme)
	_ = sourcev1.AddToScheme(scheme)
	_ = sourcev1beta2.AddToScheme(scheme)
	return scheme
}

func CheckFluxCRDs(ctx context.Context, kubeClient client.Client) error {
	ctx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()
	crdList := &apiextensionsv1.CustomResourceDefinitionList{}
	selector := client.MatchingLabels{manifestgen.PartOfLabelKey: manifestgen.PartOfLabelValue}
	err := kubeClient.List(ctx, crdList, selector)
	if err != nil {
		return err
	}
outer:
	for _, gvr := range wathedResources {
		grName := gvr.GroupResource().String()
		for _, crd := range crdList.Items {
			if crd.Name == grName {
				continue outer
			}
		}
		return fmt.Errorf("flux CRD %s not declared in API", grName)
	}
	return nil
}

func NewInformer(ctx context.Context, config *rest.Config, objEvents chan ObjEvent,
	reconcileDependents bool, namespace string) (*TreeInformer, error) {

	cl, err := dynamic.NewForConfig(config)
	if err != nil {
		return nil, fmt.Errorf("failed to create client: %s", err)
	}
	kubeClient, err := client.New(config, client.Options{Scheme: NewScheme()})
	if err != nil {
		return nil, err
	}
	// Fail promptly if there is an API connectivity issue
	if err := CheckFluxCRDs(ctx, kubeClient); err != nil {
		return nil, err
	}

	tree := TreeInformer{
		Context:             ctx,
		Client:              kubeClient,
		Initialized:         false,
		ReconcileDependents: reconcileDependents,
		ObjectMap:           NewMap(),
		ObjEvents:           objEvents,
		eventQueue:          make(chan informerEvent, 10_000),
		defaultNamespace:    namespace,
		runIdentifier:       utils.RandomString(6),
	}

	factory := dynamicinformer.NewFilteredDynamicSharedInformerFactory(cl, time.Minute, namespace, nil)
	handlers := cache.ResourceEventHandlerFuncs{
		AddFunc:    tree.resourceAdded,
		UpdateFunc: tree.resourceUpdated,
		DeleteFunc: tree.resourceDeleted,
	}
	go tree.eventHandler() // Used to buffer events from handlers

	for _, gvr := range wathedResources {
		informer := factory.ForResource(gvr)
		if _, err := informer.Informer().AddEventHandler(handlers); err != nil {
			return nil, err
		}
	}
	factory.Start(ctx.Done())
	// Wait for all the internal machinery to warm up and for cache so sync
	for gvr, ok := range factory.WaitForCacheSync(ctx.Done()) {
		if !ok {
			return nil, fmt.Errorf("failed to sync cache for resource %v", gvr)
		}
	}
	// Wait for all initial events to be handled
	for len(tree.eventQueue) > 0 {
		time.Sleep(10 * time.Millisecond)
	}

	// As UpdateDependencies processing is disabled during cache sync, do it once we have a complete view of the system.
	for _, obj := range tree.ObjectMap.Items() {
		if err := obj.UpdateDependencies(tree.ObjectMap); err != nil {
			return &tree, err
		}
	}
	// Same for ProcessReadyWithDeps
	tree.ObjectMap.ProcessReadyWithDeps()
	// Then tree can be considered as initialized
	tree.lock.Lock()
	tree.Initialized = true
	tree.lock.Unlock()
	// Request reconciliation of objects that are reconcilable
	for _, obj := range tree.ObjectMap.Items() {
		if Reconcilable(obj) && !obj.Ready() {
			tree.setReconcileStartedAnnotations(obj)
		}
	}
	return &tree, nil
}

func convertObject(u *unstructured.Unstructured, obj FluxAdapter) FluxAdapter {
	if err := runtime.DefaultUnstructuredConverter.FromUnstructured(u.UnstructuredContent(), obj.ClientObject()); err != nil {
		log.Printf("Failed to convert unstructured %s to %s\n",
			object.UnstructuredToObjMetadata(u).String(), obj.GetObjectKind().GroupVersionKind().Kind)
	}
	return obj
}

func castObject(object *unstructured.Unstructured) FluxAdapter {
	switch object.GetKind() {
	case kustomizev1.KustomizationKind:
		return convertObject(object, kustomizationAdapter{&kustomizev1.Kustomization{}})
	case helmv2.HelmReleaseKind:
		return convertObject(object, helmReleaseAdapter{&helmv2.HelmRelease{}})
	case sourcev1beta2.HelmChartKind:
		return convertObject(object, helmChartAdapter{&sourcev1beta2.HelmChart{}})
	case sourcev1beta2.HelmRepositoryKind:
		return convertObject(object, helmRepositoryAdapter{&sourcev1beta2.HelmRepository{}})
	case sourcev1.GitRepositoryKind:
		return convertObject(object, gitRepositoryAdapter{&sourcev1.GitRepository{}})
	case sourcev1beta2.OCIRepositoryKind:
		return convertObject(object, ociRepositoryAdapter{&sourcev1beta2.OCIRepository{}})
	default:
		return nil // Informer is not supposed to notify about other objects
	}
}

func (t *TreeInformer) resourceAdded(obj interface{}) {
	select {
	case t.eventQueue <- informerEvent{object: obj, delete: false}:
	default:
		log.Println("Failed to handle event, queue is full")
	}
}

func (t *TreeInformer) resourceUpdated(old, new interface{}) {
	select {
	case t.eventQueue <- informerEvent{object: new, delete: false}:
	default:
		log.Println("Failed to handle event, queue is full")
	}
}

func (t *TreeInformer) resourceDeleted(obj interface{}) {
	select {
	case t.eventQueue <- informerEvent{object: obj, delete: true}:
	default:
		log.Println("Failed to handle event, queue is full")
	}
}

func (t *TreeInformer) eventHandler() {
	for {
		select {
		case <-t.Context.Done():
			return
		case treeEvent := <-t.eventQueue:
			if u, ok := treeEvent.object.(*unstructured.Unstructured); ok {
				t.handleEvent(NewFluxObject(castObject(u), t.defaultNamespace),
					treeEvent.delete)
			}
		}
	}
}

func (t *TreeInformer) handleEvent(obj FluxObject, delete bool) {
	t.lock.Lock()
	defer t.lock.Unlock()

	if delete {
		t.ObjectMap.Remove(obj)
		t.ObjectMap.ProcessReadyWithDeps()
		t.dispatchEvent(obj, true)
		return
	}

	prev, node, err := t.ObjectMap.Store(obj, t.Initialized)
	if err != nil {
		log.Printf("Failed to store object %s: %s", obj.QualifiedName(), err)
	}
	obj.computeStatus(prev)
	if t.Initialized {
		if prev == nil || prev.Ready() != obj.Ready() {
			t.ObjectMap.ProcessReadyWithDeps()
		}
		if Reconcilable(node) && !obj.Ready() {
			t.setReconcileStartedAnnotations(obj)
		}
		if obj.Ready() && (prev == nil || !prev.Ready()) {
			t.setReconcileCompletedAnnotations(obj)
			t.NotifyReconcilableDependents(obj)
		}
		// Notify client of object changes
		if prev == nil || obj.Ready() != prev.Ready() || obj.Status() != prev.Status() ||
			obj.GetLastHandledReconcileAt() != prev.GetLastHandledReconcileAt() {
			t.dispatchEvent(obj, false)
		}
	}
}

func (t *TreeInformer) dispatchEvent(obj FluxObject, delete bool) {
	select {
	case t.ObjEvents <- ObjEvent{FluxObject: obj, Delete: delete}:
	default:
		log.Println("Failed to dispatch event, queue is full")
	}
}

func Reconcilable(node TreeNode) bool {
	if node.Ready() || node.Suspended() {
		return false
	} else {
		for _, dep := range node.GetDependencies() {
			if !dep.ReadyWithDependencies() {
				return false
			}
		}
		return true
	}
}

func (t *TreeInformer) NotifyReconcilableDependents(object FluxObject) {
	for _, node := range t.ObjectMap.Items() {
		if depends, _ := node.DependsOn(object, false); depends && Reconcilable(node) {
			t.setReconcileStartedAnnotations(node)
		}
	}
}

// We build a reconcileID using the identifier of current execution and the object generation
// It ensures that a given generation won't be reconciled more than one time per execution of sylvactl,
// but still enables a given generation to be reconciled in another execution, which can be desirable
// if some secrets or configMaps changed but not the HelmReleases of Kustomizations using them.
func (t *TreeInformer) getReconcileID(object client.Object) string {
	return fmt.Sprintf(".%d.%s", object.GetGeneration(), t.runIdentifier)
}

// Sets reconcile start annotation if it does not yet exist for current identifier
// if not, also request flux reconciliation if --reconcile option is set
func (t *TreeInformer) setReconcileStartedAnnotations(object FluxObject) {
	reconcileDate := time.Now().Format(DateFormat)
	reconcileStartedAnnotation := ReconcileStartedAnnotation + t.getReconcileID(object)
	if object.GetAnnotationValue(reconcileStartedAnnotation) == "" {
		annotations := map[string]string{
			reconcileStartedAnnotation: reconcileDate,
		}
		if t.ReconcileDependents {
			annotations[meta.ReconcileRequestAnnotation] = reconcileDate
			// Force-reconcile helmRelease if it failed to install or upgrade
			if object.GetObjectKind().GroupVersionKind().Kind == helmv2.HelmReleaseKind {
				if readyCond := apimeta.FindStatusCondition(object.GetConditions(), meta.ReadyCondition); readyCond != nil &&
					(readyCond.Reason == helmv2.InstallFailedReason || readyCond.Reason == helmv2.UpgradeFailedReason) {
					annotations[helmv2.ForceRequestAnnotation] = reconcileDate
				}
			}
		}
		go t.AnnotateObject(object.QualifiedName(), annotations)
	}
}

func (t *TreeInformer) setReconcileCompletedAnnotations(object FluxObject) {
	reconcileID := t.getReconcileID(object)
	if object.GetAnnotationValue(ReconcileStartedAnnotation+reconcileID) != "" &&
		object.GetAnnotationValue(ReconcileCompletedAnnotation+reconcileID) == "" {
		go t.AnnotateObject(object.QualifiedName(), map[string]string{
			ReconcileCompletedAnnotation + reconcileID: time.Now().Format(DateFormat),
		})
	}
}

func (t *TreeInformer) AnnotateObject(objName string, annotations map[string]string) {

	err := retry.RetryOnConflict(retry.DefaultBackoff, func() (err error) {
		obj, found := t.ObjectMap.Get(objName)
		if !found {
			return fmt.Errorf("object %s not found in map", objName)
		}
		object := obj.DeepCopyObject().(client.Object)
		patch := client.MergeFrom(object.DeepCopyObject().(client.Object))
		if ann := object.GetAnnotations(); ann == nil {
			object.SetAnnotations(annotations)
		} else {
			for annotationName, annotationValue := range annotations {
				ann[annotationName] = annotationValue
			}
			cleanOldReconcileAnnotations(ann, t.getReconcileID(object))
			object.SetAnnotations(ann)
		}
		return t.Client.Patch(t.Context, object, patch)
	})
	if err != nil {
		log.Printf("Failed to annotate object %s: %s", objName, err)
	}
}

// Remove older reconcile annotations to avoid accumulating them forever
func cleanOldReconcileAnnotations(annotations map[string]string, reconcileID string) {
	sylvactlAnnotations := sortableAnnotations{}
	for name, value := range annotations {
		if strings.HasPrefix(name, reconcileAnnotationPrefix) {
			if date, err := time.Parse(DateFormat, value); err == nil {
				sylvactlAnnotations = append(sylvactlAnnotations, sortableAnnotation{
					name: name,
					date: date,
				})
			}
		}
	}
	if len(sylvactlAnnotations) > maxAnnotations {
		sort.Sort(sylvactlAnnotations)
		for i := 0; i < len(sylvactlAnnotations)-maxAnnotations; i++ {
			delete(annotations, sylvactlAnnotations[i].name)
		}
	}
}

type sortableAnnotation struct {
	name string
	date time.Time
}
type sortableAnnotations []sortableAnnotation

func (s sortableAnnotations) Len() int {
	return len(s)
}

func (s sortableAnnotations) Less(i, j int) bool {
	return s[i].date.Before(s[j].date)
}

func (s sortableAnnotations) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
