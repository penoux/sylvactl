/*
Copyright 2024 The Sylva authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Derived work from https://github.com/fluxcd/flux2/blob/main/cmd/flux/tree_kustomization.go
Copyright 2021 The Flux authors
*/

package inventory

import (
	"bytes"
	"compress/gzip"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"strings"
	"sync"

	corev1 "k8s.io/api/core/v1"
	apiextensionsv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/rest"
	kstatus "sigs.k8s.io/cli-utils/pkg/kstatus/status"
	"sigs.k8s.io/cli-utils/pkg/object"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/apiutil"

	helmv2 "github.com/fluxcd/helm-controller/api/v2beta2"
	kustomizev1 "github.com/fluxcd/kustomize-controller/api/v1"
	ssautils "github.com/fluxcd/pkg/ssa/utils"

	"gitlab.com/sylva-projects/sylva-elements/sylvactl/internal/tree"
)

type InventoryOptions struct {
	SkipReadyResources bool
}

func getFluxInventory(ctx context.Context, u *unstructured.Unstructured, tree tree.Tree, clientConfig *rest.Config) error {

	// Then fetch inventory if it is appropriated
	var err error
	var displayInventory, inventory []*unstructured.Unstructured

	if u.GroupVersionKind() == kustomizev1.GroupVersion.WithKind(kustomizev1.KustomizationKind) {
		ks := &kustomizev1.Kustomization{}
		if err := runtime.DefaultUnstructuredConverter.FromUnstructured(u.Object, ks); err != nil {
			return fmt.Errorf("failed to convert unstructured %s to Kustomization: %w", client.ObjectKeyFromObject(u), err)
		}
		inventory, err = getKustomizationInventory(ctx, ks, clientConfig)
		if err != nil {
			return err
		}
	} else if u.GroupVersionKind() == helmv2.GroupVersion.WithKind(helmv2.HelmReleaseKind) {
		hr := &helmv2.HelmRelease{}
		if err := runtime.DefaultUnstructuredConverter.FromUnstructured(u.Object, hr); err != nil {
			return fmt.Errorf("failed to convert unstructured %s to HelmRelease: %w", client.ObjectKeyFromObject(u), err)
		}
		inventory, err = getHelmReleaseInventory(ctx, hr, clientConfig)
		if err != nil {
			return err
		}
	}

	// Build the list of namespaces on which resources were produced
	var nsList []string
	for _, obj := range inventory {
		displayInventory = append(displayInventory, obj)
		if obj.GetNamespace() != "" {
			nsList = appendUniq(nsList, obj.GetNamespace())
		}
	}
	// Get all objects from namespaces
	objMap, err := GetResourcesMap(ctx, nsList, clientConfig)
	if err != nil {
		log.Printf("Failed build resource map: %s", err)
	}
	// Add objects to tree, as well as their owned objects
	for _, obj := range displayInventory {
		var node *unstructuredNode
		// Attempt to use object from resourceMap to find owned resources
		if ownerObj, found := objMap[obj.GetUID()]; found {
			node = ownerObj
		} else {
			node = &unstructuredNode{Unstructured: obj}
		}
		childTree := tree.Add(node)
		node.Status, err = kstatus.Compute(node.Unstructured)
		if err != nil {
			log.Printf("Failed to compute status of %s %s: %s", obj.GetKind(), client.ObjectKeyFromObject(obj), err)
		}
		addOwnedNodes(node, childTree, objMap)
		if err := getFluxInventory(ctx, obj, childTree, clientConfig); err != nil {
			log.Printf("Failed to get flux inventory for %s %s: %s", obj.GetKind(), client.ObjectKeyFromObject(obj), err)
		}
	}
	return nil
}

func addOwnedNodes(obj *unstructuredNode, tree tree.Tree, objMap ResourcesMap) {
	for _, owned := range obj.Owned {
		node := tree.Add(owned)
		var err error
		if owned.Status, err = kstatus.Compute(owned.Unstructured); err != nil {
			log.Printf("Failed to compute status of %s %s: %s", obj.GetKind(), client.ObjectKeyFromObject(obj), err)
		}
		addOwnedNodes(owned, node, objMap)
	}
}

func appendUniq(s []string, e string) []string {
	for _, a := range s {
		if a == e {
			return s
		}
	}
	return append(s, e)
}

func getKustomizationInventory(ctx context.Context, kustomization *kustomizev1.Kustomization, clientConfig *rest.Config) ([]*unstructured.Unstructured, error) {

	if kustomization.Status.Inventory == nil || len(kustomization.Status.Inventory.Entries) == 0 {
		return []*unstructured.Unstructured{}, nil
	}

	// Build client from provided config
	kubeClient, err := GetClient(clientConfig)
	if err != nil {
		return nil, err
	}
	// Build and use remote client if any
	if kustomization.Spec.KubeConfig != nil {
		remoteConfig, err := getRemoteConfig(ctx, kustomization.Spec.KubeConfig, kustomization.Namespace, kubeClient)
		if err != nil {
			return nil, err
		}
		remoteClient, err := GetClient(remoteConfig)
		if err != nil {
			return nil, err
		} else {
			*clientConfig = *remoteConfig
			kubeClient = remoteClient
		}
	}

	var mu sync.Mutex
	var wg sync.WaitGroup
	var inventory []*unstructured.Unstructured
	for _, entry := range kustomization.Status.Inventory.Entries {
		// Get object to retrieve its current status in goroutine
		wg.Add(1)
		go func(ref kustomizev1.ResourceRef) {
			defer wg.Done()
			objMetadata, err := object.ParseObjMetadata(ref.ID)
			if err != nil {
				log.Printf("Failed to parse object metadata %s: %s", ref.ID, err)
				return
			}

			if objMetadata.GroupKind.Group == kustomizev1.GroupVersion.Group &&
				objMetadata.GroupKind.Kind == kustomizev1.KustomizationKind &&
				objMetadata.Namespace == kustomization.Namespace &&
				objMetadata.Name == kustomization.Name {
				return
			}

			obj := &unstructured.Unstructured{}
			obj.SetGroupVersionKind(objMetadata.GroupKind.WithVersion(ref.Version))
			nsName := client.ObjectKey{
				Namespace: objMetadata.Namespace,
				Name:      objMetadata.Name,
			}

			if err := kubeClient.Get(ctx, nsName, obj); err != nil {
				log.Printf("Failed to get object %s %s from api: %s", obj.GetKind(), nsName, err)
			}

			mu.Lock()
			inventory = append(inventory, obj)
			mu.Unlock()

		}(entry)
	}
	wg.Wait()
	return inventory, nil
}

type hrStorage struct {
	Name     string `json:"name,omitempty"`
	Manifest string `json:"manifest,omitempty"`
}

func getHelmReleaseInventory(ctx context.Context, hr *helmv2.HelmRelease, clientConfig *rest.Config) ([]*unstructured.Unstructured, error) {

	objectKey := client.ObjectKeyFromObject(hr)

	// Build client from provided config
	kubeClient, err := GetClient(clientConfig)
	if err != nil {
		return nil, err
	}
	// Build and use remote client if any
	if hr.Spec.KubeConfig != nil {
		remoteConfig, err := getRemoteConfig(ctx, hr.Spec.KubeConfig, hr.Namespace, kubeClient)
		if err != nil {
			return nil, err
		}
		remoteClient, err := GetClient(remoteConfig)
		if err != nil {
			return nil, err
		} else {
			*clientConfig = *remoteConfig
			kubeClient = remoteClient
		}
	}

	storageNamespace := hr.GetNamespace()
	if hr.Spec.StorageNamespace != "" {
		storageNamespace = hr.Spec.StorageNamespace
	}
	inventory := []*unstructured.Unstructured{}

	latest := hr.Status.History.Latest()
	if len(storageNamespace) == 0 || latest == nil {
		// Skip release if it has no current
		return inventory, nil
	}

	storageKey := client.ObjectKey{
		Namespace: storageNamespace,
		Name:      fmt.Sprintf("sh.helm.release.v1.%s.v%v", latest.Name, latest.Version),
	}

	storageSecret := &corev1.Secret{}
	if err := kubeClient.Get(ctx, storageKey, storageSecret); err != nil {
		// skip release if it has no storage
		if apierrors.IsNotFound(err) {
			return inventory, nil
		}
		return nil, fmt.Errorf("failed to find the Helm storage object for HelmRelease '%s': %w", objectKey.String(), err)
	}

	releaseData, releaseFound := storageSecret.Data["release"]
	if !releaseFound {
		return nil, fmt.Errorf("failed to decode the Helm storage object for HelmRelease '%s'", objectKey.String())
	}

	// adapted from https://github.com/helm/helm/blob/02685e94bd3862afcb44f6cd7716dbeb69743567/pkg/storage/driver/util.go
	var b64 = base64.StdEncoding
	b, err := b64.DecodeString(string(releaseData))
	if err != nil {
		return nil, err
	}
	var magicGzip = []byte{0x1f, 0x8b, 0x08}
	if bytes.Equal(b[0:3], magicGzip) {
		r, err := gzip.NewReader(bytes.NewReader(b))
		if err != nil {
			return nil, err
		}
		defer r.Close()
		b2, err := io.ReadAll(r)
		if err != nil {
			return nil, err
		}
		b = b2
	}

	// extract objects from Helm storage
	var rls hrStorage
	if err := json.Unmarshal(b, &rls); err != nil {
		return nil, fmt.Errorf("failed to decode the Helm storage object for HelmRelease '%s': %w", objectKey.String(), err)
	}

	inventory, err = ssautils.ReadObjects(strings.NewReader(rls.Manifest))
	if err != nil {
		return nil, fmt.Errorf("failed to read the Helm storage object for HelmRelease '%s': %w", objectKey.String(), err)
	}

	var wg sync.WaitGroup
	for _, obj := range inventory {
		// set the namespace on namespaced objects
		if obj.GetNamespace() == "" {
			if isNamespaced, _ := apiutil.IsObjectNamespaced(obj, kubeClient.Scheme(), kubeClient.RESTMapper()); isNamespaced {
				if hr.Spec.TargetNamespace != "" {
					obj.SetNamespace(hr.Spec.TargetNamespace)
				} else {
					obj.SetNamespace(hr.GetNamespace())
				}
			}
		}
		// Get object to retrieve its current status
		wg.Add(1)
		go func(u *unstructured.Unstructured) {
			defer wg.Done()
			if err := kubeClient.Get(ctx, client.ObjectKeyFromObject(u), u); err != nil {
				log.Printf("Failed to get object %s %s from api: %s", u.GetKind(), client.ObjectKeyFromObject(u), err)
			}
		}(obj)
	}
	wg.Wait()

	// search for CRDs managed by the HelmRelease if installing or upgrading CRDs is enabled in spec
	if (hr.Spec.Install != nil && len(hr.Spec.Install.CRDs) > 0 && hr.Spec.Install.CRDs != helmv2.Skip) ||
		(hr.Spec.Upgrade != nil && len(hr.Spec.Upgrade.CRDs) > 0 && hr.Spec.Upgrade.CRDs != helmv2.Skip) {
		selector := client.MatchingLabels{
			fmt.Sprintf("%s/name", helmv2.GroupVersion.Group):      hr.GetName(),
			fmt.Sprintf("%s/namespace", helmv2.GroupVersion.Group): hr.GetNamespace(),
		}
		crdKind := "CustomResourceDefinition"
		var list apiextensionsv1.CustomResourceDefinitionList
		if err := kubeClient.List(ctx, &list, selector); err == nil {
			for _, crd := range list.Items {
				found := false
				for _, r := range inventory {
					if r.GetName() == crd.GetName() && r.GetKind() == crdKind {
						found = true
						break
					}
				}
				if !found {
					obj, err := runtime.DefaultUnstructuredConverter.ToUnstructured(crd)
					if err != nil {
						continue
					}
					inventory = append(inventory, &unstructured.Unstructured{Object: obj})
				}
			}
		}
	}
	return inventory, nil
}
