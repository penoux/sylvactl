/*
Copyright 2023 The Sylva authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"log"
	"os"

	"github.com/spf13/cobra"

	"github.com/go-logr/logr"
	"k8s.io/cli-runtime/pkg/genericclioptions"
	ctrllog "sigs.k8s.io/controller-runtime/pkg/log"
)

var (
	defaultTimeout = "30s"
	rootCmd        = &cobra.Command{
		Use:          "sylvactl",
		SilenceUsage: true,
	}
	kubeconfigArgs = genericclioptions.NewConfigFlags(false)
)

func init() {
	log.SetFlags(0)
	log.SetOutput(os.Stderr)
	// Prevent controller-runtime from complaining that logger was not initialized
	ctrllog.SetLogger(logr.New(ctrllog.NullLogSink{}))
	kubeconfigArgs.Timeout = &defaultTimeout
	kubeconfigArgs.AddFlags(rootCmd.PersistentFlags())
	// Disable client rate limits to avoid warnings
	kubeconfigArgs.WithDiscoveryBurst(-1).WithDiscoveryQPS(-1)
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
